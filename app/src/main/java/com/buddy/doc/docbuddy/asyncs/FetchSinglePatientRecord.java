package com.buddy.doc.docbuddy.asyncs;

import android.os.AsyncTask;

import com.buddy.doc.docbuddy.database.PatientRecord;
import com.buddy.doc.docbuddy.interfaces.PatientRecordOperationListener;

/**
 * Created by Chetan on 6/15/2017.
 */

public class FetchSinglePatientRecord extends AsyncTask<Long, Void, PatientRecord> {
    private PatientRecordOperationListener patientRecordOperationListener;

    public FetchSinglePatientRecord(PatientRecordOperationListener patientRecordOperationListener) {
        this.patientRecordOperationListener = patientRecordOperationListener;
    }

    @Override
    protected PatientRecord doInBackground(Long... params) {
        try {
            return PatientRecord.findById(PatientRecord.class, params[0]);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(PatientRecord patientRecord) {
        patientRecordOperationListener.onSinglePatientRecordFetched(patientRecord);
    }
}
