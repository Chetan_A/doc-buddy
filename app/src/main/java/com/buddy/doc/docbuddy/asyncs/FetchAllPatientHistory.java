package com.buddy.doc.docbuddy.asyncs;

import android.os.AsyncTask;

import com.buddy.doc.docbuddy.database.PatientRecord;
import com.buddy.doc.docbuddy.interfaces.PatientRecordOperationListener;

import java.util.List;

/**
 * Created by Chetan on 6/15/2017.
 */

public class FetchAllPatientHistory extends AsyncTask<Void, Void, List<PatientRecord>> {
    private PatientRecordOperationListener listener;

    public FetchAllPatientHistory(PatientRecordOperationListener listener) {
        this.listener = listener;
    }

    @Override
    protected List<PatientRecord> doInBackground(Void... params) {
        return PatientRecord.listAll(PatientRecord.class);
    }

    @Override
    protected void onPostExecute(List<PatientRecord> patientRecords) {
        listener.onMultiplePatientRecordsFetched(patientRecords);
    }
}
