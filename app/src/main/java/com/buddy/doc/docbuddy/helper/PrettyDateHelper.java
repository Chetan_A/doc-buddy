package com.buddy.doc.docbuddy.helper;

import java.util.Date;

/**
 * Created by Chetan on 6/15/2017.
 */

public class PrettyDateHelper {

    public PrettyDateHelper() {

    }

    public static String toString(Date date) {

        long current = (new Date()).getTime(),
                timestamp = date.getTime(),
                diff = (current - timestamp) / 1000;
        int amount;
        String what;

        /**
         * Second counts
         * 3600: hour
         * 86400: day
         * 604800: week
         * 2592000: month
         * 31536000: year
         */

        if (diff > 31536000) {
            amount = (int) (diff / 31536000);
            what = "years ago";
        } else if (diff > 31536000) {
            amount = (int) (diff / 31536000);
            what = "months ago";
        } else if (diff > 604800) {
            amount = (int) (diff / 604800);
            what = "weeks ago";
        } else if (diff > 86400) {
            amount = (int) (diff / 86400);
            what = "days ago";
        } else if (diff > 3600) {
            amount = (int) (diff / 3600);
            what = "hours ago";
        } else if (diff > 60) {
            amount = (int) (diff / 60);
            what = "minutes ago";
        } else {
            amount = (int) diff;
            what = "s";
            if (amount < 6) {
                return "Just now";
            }
        }

        return amount + what;
    }

}
