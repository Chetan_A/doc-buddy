package com.buddy.doc.docbuddy.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.buddy.doc.docbuddy.R;
import com.buddy.doc.docbuddy.databinding.ActivityLoginBinding;

import static com.buddy.doc.docbuddy.helper.AppConstants.IS_LOGGED_IN;
import static com.buddy.doc.docbuddy.helper.AppConstants.LOGIN_SESSION;
import static com.buddy.doc.docbuddy.helper.AppConstants.USERNAME;

public class LoginActivity extends AppCompatActivity {
    private static final String DEMO_USER_NAME = "User";
    private ActivityLoginBinding binding;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

        SharedPreferences loginSession = getApplicationContext().getSharedPreferences(LOGIN_SESSION, MODE_PRIVATE);
        if (loginSession.getBoolean(IS_LOGGED_IN, false)) {
            startMainActivity(loginSession.getString(USERNAME, DEMO_USER_NAME));
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        binding.loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                saveSession(DEMO_USER_NAME);//TODO testing purpose
                //checkFieldsAndPerformLogin();
            }
        });
    }

    private void checkFieldsAndPerformLogin() {
        Boolean isVerified = true;
        View focusedView = null;

        if (isEmpty(binding.userName)) {
            binding.userName.setError(getString(R.string.no_username_error));
            isVerified = false;
            focusedView = binding.userName;
        } else {
            binding.userName.setError(null);
        }

        if (isEmpty(binding.password)) {
            binding.password.setError(getString(R.string.no_password_error));
            isVerified = false;
            if (focusedView == null) {
                focusedView = binding.password;
            } else {
                binding.password.setError(null);
            }
        }

        if (isVerified) {
            saveSession(binding.userName.getText().toString());
        } else {
            if (focusedView != null) {
                focusedView.requestFocus();
            }
        }
    }

    private void saveSession(String userName) {
        SharedPreferences loginSession = getApplicationContext().getSharedPreferences(LOGIN_SESSION, MODE_PRIVATE);
        SharedPreferences.Editor editor = loginSession.edit();
        editor.putBoolean(IS_LOGGED_IN, true);
        editor.putString(USERNAME, userName);
        editor.apply();
        startMainActivity(userName);
    }

    private void startMainActivity(String userName) {
        Intent intent = new Intent(mContext, MainActivity.class);
        intent.putExtra(USERNAME, userName);
        startActivity(intent);
        finish();
    }

    private boolean isEmpty(EditText editText) {
        return editText.getText().toString().isEmpty() || editText.getText().toString().length() < 3;
    }
}
