package com.buddy.doc.docbuddy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.buddy.doc.docbuddy.interfaces.PatientHistoryClickListener;
import com.buddy.doc.docbuddy.database.PatientRecord;
import com.buddy.doc.docbuddy.helper.PrettyDateHelper;
import com.buddy.doc.docbuddy.R;

import java.util.ArrayList;

/**
 * Created by Chetan on 6/15/2017.
 */

public class PatientHistoryAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private ArrayList<PatientRecord> patientRecordArrayList;
    private PatientHistoryClickListener patientHistoryClickListener;

    public PatientHistoryAdapter(Context mContext, ArrayList<PatientRecord> patientRecordArrayList,
                                 PatientHistoryClickListener patientHistoryClickListener) {
        this.mContext = mContext;
        this.patientRecordArrayList = patientRecordArrayList;
        this.patientHistoryClickListener = patientHistoryClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.patient_single_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof MyViewHolder) {
            MyViewHolder holder = (MyViewHolder) viewHolder;
            holder.patientname.setText(patientRecordArrayList.get(position).patientName);
            holder.likelihood.setText(patientRecordArrayList.get(position).likelihood + " %");
            holder.scanDate.setText(PrettyDateHelper.toString(patientRecordArrayList.get(position).dateScanned));

            holder.bind(patientRecordArrayList.get(position).getId(),patientRecordArrayList.get(position).likelihood );
        }

    }

    @Override
    public int getItemCount() {
        return patientRecordArrayList.size();
    }

    private class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView patientname;
        private TextView likelihood;
        private TextView scanDate;

        public MyViewHolder(View itemView) {
            super(itemView);
            patientname = (TextView) itemView.findViewById(R.id.patient_name);
            likelihood = (TextView) itemView.findViewById(R.id.likelihood);
            scanDate = (TextView) itemView.findViewById(R.id.scan_date);

        }

        public void bind(final Long id, Integer likelihoodInt) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    patientHistoryClickListener.onHistoryViewClicked(id);
                }
            });
            switch (likelihoodInt) {

                case 0:
                    likelihood.setTextColor(mContext.getResources().getColor(R.color.colorSuccess));
                    break;
                case 25:

                    likelihood.setTextColor(mContext.getResources().getColor(R.color.colorWarningTwo));
                    break;
                case 50:

                    likelihood.setTextColor(mContext.getResources().getColor(R.color.colorWarning));
                    break;
                case 75:

                    likelihood.setTextColor(mContext.getResources().getColor(R.color.colorRed));
                    break;
                case 100:

                    likelihood.setTextColor(mContext.getResources().getColor(R.color.colorFailure));
                    break;
                default:
                    Toast.makeText(mContext, R.string.generic_error_message, Toast.LENGTH_SHORT).show();
                    break;
            }


        }
    }
}
