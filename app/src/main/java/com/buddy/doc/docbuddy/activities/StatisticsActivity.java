package com.buddy.doc.docbuddy.activities;

import android.databinding.DataBindingUtil;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.buddy.doc.docbuddy.asyncs.FetchAllPatientHistory;
import com.buddy.doc.docbuddy.database.PatientRecord;
import com.buddy.doc.docbuddy.interfaces.PatientRecordOperationListener;
import com.buddy.doc.docbuddy.R;
import com.buddy.doc.docbuddy.databinding.ActivityStatisticsBinding;

import java.util.List;

public class StatisticsActivity extends AppCompatActivity {
    private ActivityStatisticsBinding binding;

    private PatientRecordOperationListener listener = new PatientRecordOperationListener() {
        @Override
        public void onPatientRecordSaved(PatientRecord savedPatientRecord) {

        }

        @Override
        public void onPatientRecordDeleted() {

        }

        @Override
        public void onSinglePatientRecordFetched(PatientRecord fetchedPatientRecord) {

        }

        @Override
        public void onMultiplePatientRecordsFetched(List<PatientRecord> fetchedPatientRecordList) {
            displayStatistics(fetchedPatientRecordList);
        }
    };

    private void displayStatistics(List<PatientRecord> fetchedPatientRecordList) {
        binding.totalScansHeader.setText(getString(R.string.total_patients) + " " + fetchedPatientRecordList.size());

        int healthy = 0;
        int levelOne = 0;
        int levelTwo = 0;
        int levelThree = 0;
        int levelFour = 0;
        for (int i = 0; i < fetchedPatientRecordList.size(); i++) {
            switch (fetchedPatientRecordList.get(i).likelihood) {
                case 0:
                    healthy++;
                    break;
                case 25:
                    levelOne++;
                    break;
                case 50:
                    levelTwo++;
                    break;
                case 75:
                    levelThree++;
                    break;
                case 100:
                    levelFour ++;
                    break;
                default:
                    break;
            }
        }
        binding.healthyPatientsHeader.setText(getString(R.string.healthy_patients) + " " + healthy);
        binding.levelOneHeader.setText(getString(R.string.level_one_patients) + " " + levelOne);
        binding.levelTwoHeader.setText(getString(R.string.level_two_patients) + " " + levelTwo);
        binding.levelThreeHeader.setText(getString(R.string.level_three_patients) + " " + levelThree);
        binding.levelFourHeader.setText(getString(R.string.level_four_patients) + " " + levelFour);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_statistics);
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.stats);
        actionBar.setDisplayHomeAsUpEnabled(true);
        new FetchAllPatientHistory(listener).execute();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
