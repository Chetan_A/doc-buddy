package com.buddy.doc.docbuddy.interfaces;

import com.buddy.doc.docbuddy.database.PatientRecord;

import java.util.List;

/**
 * Created by Chetan on 6/15/2017.
 */

public interface PatientRecordOperationListener {
    void onPatientRecordSaved(PatientRecord savedPatientRecord);

    void onPatientRecordDeleted();

    void onSinglePatientRecordFetched(PatientRecord fetchedPatientRecord);

    void onMultiplePatientRecordsFetched(List<PatientRecord> fetchedPatientRecordList);
}
