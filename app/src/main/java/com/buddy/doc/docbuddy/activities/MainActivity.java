package com.buddy.doc.docbuddy.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.buddy.doc.docbuddy.R;
import com.buddy.doc.docbuddy.databinding.ActivityMainBinding;

import static com.buddy.doc.docbuddy.helper.AppConstants.IS_LOGGED_IN;
import static com.buddy.doc.docbuddy.helper.AppConstants.LOGIN_SESSION;
import static com.buddy.doc.docbuddy.helper.AppConstants.USERNAME;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private ActivityMainBinding binding;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(binding.appBarLayout.toolbar);

        View headerLayout = binding.navView.getHeaderView(0); // 0-index header
        TextView userName = (TextView) headerLayout.findViewById(R.id.nav_user_name);
        userName.setText("Welcome " + getIntent().getStringExtra(USERNAME));
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout, binding.appBarLayout.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        binding.drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        setOnClickListeners();
        binding.navView.setNavigationItemSelectedListener(this);
    }

    private void setOnClickListeners() {

        binding.appBarLayout.contentMain.newPatientEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNewPatientEntryActivity();
            }
        });

        binding.appBarLayout.contentMain.viewHistoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startViewHistoryActivity();
            }
        });
    }

    private void startViewHistoryActivity() {
        startActivity(new Intent(mContext, ViewPatientHistoryActivity.class));
    }

    private void startNewPatientEntryActivity() {
        startActivity(new Intent(mContext, NewPatientEntryActivity.class));
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            SharedPreferences loginSession = getApplicationContext().getSharedPreferences(LOGIN_SESSION, MODE_PRIVATE);
            SharedPreferences.Editor editor = loginSession.edit();
            editor.putBoolean(IS_LOGGED_IN, false);
            editor.remove(USERNAME);

            startLoginActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startLoginActivity() {
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_new_patient_entry) {
            startNewPatientEntryActivity();
        } else if (id == R.id.nav_view_history) {
            startViewHistoryActivity();
        } else if (id == R.id.nav_view_stats) {
            startStatisticsActivity();
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void startStatisticsActivity() {
        startActivity(new Intent(mContext, StatisticsActivity.class));
    }
}
