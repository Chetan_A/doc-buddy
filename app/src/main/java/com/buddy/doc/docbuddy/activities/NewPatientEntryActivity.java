package com.buddy.doc.docbuddy.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.buddy.doc.docbuddy.asyncs.FetchSinglePatientRecord;
import com.buddy.doc.docbuddy.helper.LikelihoodCalculator;
import com.buddy.doc.docbuddy.database.PatientRecord;
import com.buddy.doc.docbuddy.interfaces.PatientRecordOperationListener;
import com.buddy.doc.docbuddy.R;
import com.buddy.doc.docbuddy.asyncs.SavePatientRecordAsync;
import com.buddy.doc.docbuddy.databinding.ActivityNewPatientEntryBinding;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.buddy.doc.docbuddy.helper.AppConstants.LIKELIHOOD;
import static com.buddy.doc.docbuddy.helper.AppConstants.MIGRAINE_COMPLAINT;
import static com.buddy.doc.docbuddy.helper.AppConstants.PATIENT_AGE;
import static com.buddy.doc.docbuddy.helper.AppConstants.PATIENT_BIRTH_DATE;
import static com.buddy.doc.docbuddy.helper.AppConstants.PATIENT_GENDER;
import static com.buddy.doc.docbuddy.helper.AppConstants.PATIENT_NAME;
import static com.buddy.doc.docbuddy.helper.AppConstants.PATIENT_RECORD_ID;
import static com.buddy.doc.docbuddy.helper.AppConstants.TAKE_X_DRUG;

public class NewPatientEntryActivity extends AppCompatActivity {

    private ActivityNewPatientEntryBinding binding;
    private Context mContext;
    private Date birthDateSelected;

    private PatientRecordOperationListener patientRecordOperationListener = new PatientRecordOperationListener() {
        @Override
        public void onPatientRecordSaved(PatientRecord savedPatientRecord) {
            if (savedPatientRecord != null) {
                Toast.makeText(mContext, R.string.patient_record_saved, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onPatientRecordDeleted() {

        }

        @Override
        public void onSinglePatientRecordFetched(PatientRecord fetchedPatientRecord) {
            if (fetchedPatientRecord != null) {
                setForm(fetchedPatientRecord);
                disableForm();
            }

        }

        @Override
        public void onMultiplePatientRecordsFetched(List<PatientRecord> fetchedPatientRecordList) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_patient_entry);
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.check_likelihood);
        binding.result.setVisibility(View.GONE);
        if (getIntent() != null && getIntent().hasExtra(PATIENT_RECORD_ID)) {
            new FetchSinglePatientRecord(patientRecordOperationListener).execute(getIntent().getLongExtra(PATIENT_RECORD_ID, 0));
        }
        binding.birthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);
                c.set(Calendar.HOUR_OF_DAY, 23);
                c.set(Calendar.MINUTE, 59);
                c.set(Calendar.SECOND, 59);

                new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        if (!c.before(calendar)) {
                            birthDateSelected = calendar.getTime();
                            binding.birthDate.setText(new SimpleDateFormat("dd MMM yyyy").format(birthDateSelected));
                        } else {
                            Toast.makeText(mContext, R.string.date_select_error, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, year, month, day).show();
            }
        });

        binding.checkLikelyHood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.checkLikelyHood.setEnabled(false);
                checkFields();
            }
        });
    }

    private void checkFields() {
        Boolean isVerified = true;
        View focusedView = null;

        if (isEmpty(binding.patientName)) {
            binding.patientName.setError(getString(R.string.no_name_error));
            isVerified = false;
            focusedView = binding.patientName;
        } else {
            binding.patientName.setError(null);
        }

        if (isEmpty(binding.birthDate)) {
            binding.birthDate.setError(getString(R.string.no_date_error));
            isVerified = false;
        } else {
            binding.birthDate.setError(null);
        }

        if (binding.genderRadioGroup.getCheckedRadioButtonId() <= 0) {
            binding.selectOther.setError(getString(R.string.select_gender_error));
            isVerified = false;
        } else {
            binding.selectOther.setError(null);
        }

        if (isVerified) {
            checkLikelihoodOfInfection(binding.genderRadioGroup.getCheckedRadioButtonId());
        } else {
            binding.checkLikelyHood.setEnabled(true);
            if (focusedView != null) {
                focusedView.requestFocus();
            }
        }

    }

    private void checkLikelihoodOfInfection(int checkedRadioButtonId) {

        Map<String, Object> patientReport = new HashMap<>();
        patientReport.put(PATIENT_NAME, binding.patientName.getText().toString());
        patientReport.put(PATIENT_GENDER, checkedRadioButtonId == R.id.select_female ? "Female" :
                checkedRadioButtonId == R.id.select_male ? "Male" : "Other");
        patientReport.put(PATIENT_BIRTH_DATE, birthDateSelected);
        patientReport.put(PATIENT_AGE, getAge(birthDateSelected));
        patientReport.put(MIGRAINE_COMPLAINT, binding.selectMigraine.isChecked());
        patientReport.put(TAKE_X_DRUG, binding.selectXDrug.isChecked());
        Integer likelihood = new LikelihoodCalculator(patientReport).getLikelihood();
        patientReport.put(LIKELIHOOD, likelihood);
        disableForm();
        new SavePatientRecordAsync(patientRecordOperationListener).execute(patientReport);

        setLikelyResult(likelihood);

    }

    private void setLikelyResult(Integer likelihood) {
        switch (likelihood) {

            case 0:
                binding.result.setVisibility(View.VISIBLE);
                binding.result.setText(R.string.zero_probability_message);
                binding.result.setTextColor(mContext.getResources().getColor(R.color.colorSuccess));
                break;
            case 25:
                binding.result.setVisibility(View.VISIBLE);
                binding.result.setText(R.string.level_one_probability_message);
                binding.result.setTextColor(mContext.getResources().getColor(R.color.colorWarningTwo));
                break;
            case 50:
                binding.result.setVisibility(View.VISIBLE);
                binding.result.setText(R.string.level_two_probability_message);
                binding.result.setTextColor(mContext.getResources().getColor(R.color.colorWarning));
                break;
            case 75:
                binding.result.setVisibility(View.VISIBLE);
                binding.result.setText(R.string.level_three_probability_message);
                binding.result.setTextColor(mContext.getResources().getColor(R.color.colorRed));
                break;
            case 100:
                binding.result.setVisibility(View.VISIBLE);
                binding.result.setText(R.string.level_four_probability_message);
                binding.result.setTextColor(mContext.getResources().getColor(R.color.colorFailure));
                break;
            default:
                Toast.makeText(mContext, R.string.generic_error_message, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void disableForm() {
        binding.patientName.setEnabled(false);
        //binding.genderRadioGroup.setEnabled(false);

        for (int i = 0; i < binding.genderRadioGroup.getChildCount(); i++) {
            binding.genderRadioGroup.getChildAt(i).setEnabled(false);
        }
        binding.checkLikelyHood.setEnabled(false);
        binding.birthDate.setEnabled(false);
        binding.selectMigraine.setEnabled(false);
        binding.selectXDrug.setEnabled(false);
    }

    private void resetForm() {
        binding.checkLikelyHood.setEnabled(true);

        binding.patientName.setEnabled(true);
        binding.patientName.setText("");

        binding.birthDate.setEnabled(true);
        binding.birthDate.setText("");

        for (int i = 0; i < binding.genderRadioGroup.getChildCount(); i++) {
            binding.genderRadioGroup.getChildAt(i).setEnabled(true);
        }

        binding.genderRadioGroup.setEnabled(true);
        binding.genderRadioGroup.clearCheck();

        binding.selectMigraine.setEnabled(true);
        binding.selectMigraine.setChecked(false);

        binding.selectXDrug.setEnabled(true);
        binding.selectXDrug.setChecked(false);
        binding.result.setVisibility(View.GONE);
    }

    private Object getAge(Date birthDateSelected) {

        return (new Date().getTime() - birthDateSelected.getTime()) / DateUtils.YEAR_IN_MILLIS;
    }

    private boolean isEmpty(EditText editText) {
        return editText.getText().toString().isEmpty() || editText.getText().toString().length() < 1;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_patient_entry, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (item.getItemId() == R.id.action_reset) {
            resetForm();
        }
        return super.onOptionsItemSelected(item);
    }


    private void setForm(PatientRecord patientRecort) {
        binding.patientName.setText(patientRecort.patientName);
        binding.birthDate.setText(new SimpleDateFormat("dd MMM yyyy").format(patientRecort.patientBirthDate));
        binding.selectMigraine.setChecked(patientRecort.migraineComplaint);
        binding.selectXDrug.setChecked(patientRecort.takeXDrug);

        switch (patientRecort.patientGender) {
            case "Male":
                binding.selectMale.setChecked(true);
                break;
            case "Female":
                binding.selectFemale.setChecked(true);
                break;
            case "Other":
                binding.selectOther.setChecked(true);
                break;
            default:
                break;
        }
        setLikelyResult(patientRecort.likelihood);

    }
}
