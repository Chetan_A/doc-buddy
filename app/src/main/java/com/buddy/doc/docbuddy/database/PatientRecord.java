package com.buddy.doc.docbuddy.database;

import com.orm.SugarRecord;

import java.util.Date;

/**
 * Created by Chetan on 6/15/2017.
 */

public class PatientRecord extends SugarRecord {

    public String patientName;
    public String patientGender;
    public Date patientBirthDate;
    public Integer patientAge;
    public Boolean migraineComplaint;
    public Boolean takeXDrug;
    public Integer likelihood;
    public Date dateScanned;

    public PatientRecord() {

    }

    public PatientRecord(String patientName, String patientGender, Date patientBirthDate, Integer patientAge,
                         Boolean migraineComplaint, Boolean takeXDrug, Integer likelihood) {
        this.patientName = patientName;
        this.patientGender = patientGender;
        this.patientBirthDate = patientBirthDate;
        this.patientAge = patientAge;
        this.migraineComplaint = migraineComplaint;
        this.takeXDrug = takeXDrug;
        this.likelihood = likelihood;
        this.dateScanned = new Date();
    }
}
