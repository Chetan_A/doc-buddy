package com.buddy.doc.docbuddy.helper;

/**
 * Created by Chetan on 6/14/2017.
 */

public class AppConstants {

    public static final String LOGIN_SESSION = "com.buddy.doc.docbuddy.LOGIN_SESSION";
    public static final String IS_LOGGED_IN = "com.buddy.doc.docbuddy.IS_LOGGED_IN";
    public static final String USERNAME = "com.buddy.doc.docbuddy.USERNAME";


    public static final String PATIENT_NAME = "com.buddy.doc.docbuddy.PATIENT_NAME";
    public static final String PATIENT_GENDER = "com.buddy.doc.docbuddy.PATIENT_GENDER";
    public static final String PATIENT_BIRTH_DATE = "com.buddy.doc.docbuddy.PATIENT_BIRTH_DATE";
    public static final String PATIENT_AGE = "com.buddy.doc.docbuddy.PATIENT_AGE";
    public static final String MIGRAINE_COMPLAINT = "com.buddy.doc.docbuddy.MIGRAINE_COMPLAINT";
    public static final String TAKE_X_DRUG = "com.buddy.doc.docbuddy.TAKE_X_DRUG";
    public static final String LIKELIHOOD = "com.buddy.doc.docbuddy.LIKELIHOOD";
    public static final String PATIENT_RECORD_ID = "com.buddy.doc.docbuddy.PATIENT_RECORD_ID";
}
