package com.buddy.doc.docbuddy.helper;

import java.util.HashMap;
import java.util.Map;

import static com.buddy.doc.docbuddy.helper.AppConstants.MIGRAINE_COMPLAINT;
import static com.buddy.doc.docbuddy.helper.AppConstants.PATIENT_AGE;
import static com.buddy.doc.docbuddy.helper.AppConstants.PATIENT_GENDER;
import static com.buddy.doc.docbuddy.helper.AppConstants.TAKE_X_DRUG;

/**
 * Created by Chetan on 6/15/2017.
 */

public class LikelihoodCalculator {
    private Map<String, Object> patientData = new HashMap<>();

    public LikelihoodCalculator(Map<String, Object> patientData) {
        this.patientData.clear();
        this.patientData.putAll(patientData);
    }

    public Integer getLikelihood() {
        Integer likelihood = 0;

        if ( patientData.get(PATIENT_GENDER).equals("Male")) {
            likelihood += 25;
        }

        if ((Long) patientData.get(PATIENT_AGE) <= 15) {
            likelihood += 25;
        }

        if ((Boolean) patientData.get(MIGRAINE_COMPLAINT)) {
            likelihood += 25;
        }
        if ((Boolean) patientData.get(TAKE_X_DRUG)) {
            likelihood += 25;
        }

        return likelihood;

    }
}
