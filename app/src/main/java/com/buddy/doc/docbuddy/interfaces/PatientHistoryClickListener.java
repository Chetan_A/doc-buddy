package com.buddy.doc.docbuddy.interfaces;

/**
 * Created by Chetan on 6/15/2017.
 */

public interface PatientHistoryClickListener {

    void onHistoryDeleteClicked(int position, String Id);

    void onHistoryViewClicked(Long Id);

}
