package com.buddy.doc.docbuddy.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.buddy.doc.docbuddy.asyncs.FetchAllPatientHistory;
import com.buddy.doc.docbuddy.adapters.PatientHistoryAdapter;
import com.buddy.doc.docbuddy.interfaces.PatientHistoryClickListener;
import com.buddy.doc.docbuddy.database.PatientRecord;
import com.buddy.doc.docbuddy.interfaces.PatientRecordOperationListener;
import com.buddy.doc.docbuddy.R;
import com.buddy.doc.docbuddy.databinding.ActivityViewPatientHistoryBinding;

import java.util.ArrayList;
import java.util.List;

import static com.buddy.doc.docbuddy.helper.AppConstants.PATIENT_RECORD_ID;

public class ViewPatientHistoryActivity extends AppCompatActivity {
    private Context mContext;
    private RecyclerView.Adapter patientHistoryAdapter;

    private ArrayList<PatientRecord> patientRecordArrayList = new ArrayList<>();

    private PatientRecordOperationListener listener = new PatientRecordOperationListener() {
        @Override
        public void onPatientRecordSaved(PatientRecord savedPatientRecord) {

        }

        @Override
        public void onPatientRecordDeleted() {

        }

        @Override
        public void onSinglePatientRecordFetched(PatientRecord fetchedPatientRecord) {

        }

        @Override
        public void onMultiplePatientRecordsFetched(List<PatientRecord> fetchedPatientRecordList) {
            patientRecordArrayList.clear();
            patientRecordArrayList.addAll(fetchedPatientRecordList);
            patientHistoryAdapter.notifyDataSetChanged();
        }
    };

    private PatientHistoryClickListener patientHistoryClickListener = new PatientHistoryClickListener() {
        @Override
        public void onHistoryDeleteClicked(int position, String Id) {

        }

        @Override
        public void onHistoryViewClicked(Long id) {
            Intent intent = new Intent(mContext, NewPatientEntryActivity.class);
            intent.putExtra(PATIENT_RECORD_ID, id);
            startActivity(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        ActivityViewPatientHistoryBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_view_patient_history);
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.patient_history);
        binding.patientHistoryRecyclerView.setNestedScrollingEnabled(false);
        binding.patientHistoryRecyclerView.setHasFixedSize(true);
        binding.patientHistoryRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        patientHistoryAdapter = new PatientHistoryAdapter(mContext, patientRecordArrayList, patientHistoryClickListener);
        binding.patientHistoryRecyclerView.setAdapter(patientHistoryAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        new FetchAllPatientHistory(listener).execute();
        super.onResume();
    }
}
