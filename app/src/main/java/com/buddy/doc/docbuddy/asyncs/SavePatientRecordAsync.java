package com.buddy.doc.docbuddy.asyncs;

import android.os.AsyncTask;

import com.buddy.doc.docbuddy.database.PatientRecord;
import com.buddy.doc.docbuddy.interfaces.PatientRecordOperationListener;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.buddy.doc.docbuddy.helper.AppConstants.LIKELIHOOD;
import static com.buddy.doc.docbuddy.helper.AppConstants.MIGRAINE_COMPLAINT;
import static com.buddy.doc.docbuddy.helper.AppConstants.PATIENT_AGE;
import static com.buddy.doc.docbuddy.helper.AppConstants.PATIENT_BIRTH_DATE;
import static com.buddy.doc.docbuddy.helper.AppConstants.PATIENT_GENDER;
import static com.buddy.doc.docbuddy.helper.AppConstants.PATIENT_NAME;
import static com.buddy.doc.docbuddy.helper.AppConstants.TAKE_X_DRUG;

/**
 * Created by Chetan on 6/15/2017.
 */

public class SavePatientRecordAsync extends AsyncTask<Map<String, Object>, Void, PatientRecord> {
    private PatientRecordOperationListener listener;

    public SavePatientRecordAsync(PatientRecordOperationListener listener) {
        this.listener = listener;
    }

    @Override
    protected PatientRecord doInBackground(Map<String, Object>... params) {
        HashMap<String, Object> patientDate = (HashMap<String, Object>) params[0];
        PatientRecord patientRecord = new PatientRecord(
                (String) patientDate.get(PATIENT_NAME),
                (String) patientDate.get(PATIENT_GENDER),
                (Date) patientDate.get(PATIENT_BIRTH_DATE),
                ((Long) patientDate.get(PATIENT_AGE)).intValue(),
                (Boolean) patientDate.get(MIGRAINE_COMPLAINT),
                (Boolean) patientDate.get(TAKE_X_DRUG),
                (Integer) patientDate.get(LIKELIHOOD));
        try {
            patientRecord.save();
            return patientRecord;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(PatientRecord patientRecord) {
        listener.onPatientRecordSaved(patientRecord);
    }
}
